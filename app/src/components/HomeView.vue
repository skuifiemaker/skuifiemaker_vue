<template>
  <VContainer>
    <VRow>
      <VCol>
        <VCard>
          <VCardTitle>Enter liturgy</VCardTitle>
          <VCardSubtitle>
            It will be parsed automatically. Lines that don't look like song
            references will be ignored. Check the results manually.
          </VCardSubtitle>
          <VCardText>
            <VTextarea v-model="store.liturgyInput"></VTextarea>
          </VCardText>
        </VCard>
      </VCol>

      <VCol v-if="songReferencesWithAvailableVersifications.length">
        <VCard>
          <VCardTitle>Choose versifications</VCardTitle>
          <VCardSubtitle>
            Some songs might have more than one version, e.g. Psalms by Totius
            and Cloete. Choose one versification per song.
          </VCardSubtitle>
          <VCardText
            v-for="(
              { songReference, versifications }, index
            ) in songReferencesWithAvailableVersifications"
            :key="index"
          >
            <p>
              {{
                renderSongReference(
                  songReference.book,
                  songReference.number,
                  songReference.stanzas,
                )
              }}
            </p>
            <VRadioGroup
              v-if="versifications.length"
              :model-value="store.versificationIds[index]"
              @update:model-value="setVersificationId(index, $event)"
            >
              <VRadio
                v-for="v in versifications"
                :key="v.id"
                :label="renderVersification(v.year, v.author)"
                :value="v.id"
              ></VRadio>
            </VRadioGroup>
            <p v-else>No available versifications.</p>
          </VCardText>
        </VCard>
      </VCol>
    </VRow>

    <VRow v-if="songReferencesWithSelectedVersifications.length">
      <VCol>
        <VCard>
          <VCardTitle>Preview lyrics</VCardTitle>
          <VExpansionPanels class="pa-4">
            <VExpansionPanel
              v-for="(ref, index) in songReferencesWithSelectedVersifications"
              :key="index"
            >
              <LyricsPreview :song-reference="ref"></LyricsPreview>
            </VExpansionPanel>
          </VExpansionPanels>
        </VCard>
      </VCol>
    </VRow>

    <VRow v-if="songReferencesWithSelectedVersifications.length">
      <VCol>
        <VCard>
          <VCardTitle>Download all slides</VCardTitle>
          <VCardSubtitle>
            For SVG and PNG formats, you will get a zip file with numbered files
            inside.
          </VCardSubtitle>
          <VCardActions>
            <VSpacer></VSpacer>
            <VBtn :loading="isLoadingPdf" @click="makePdf()">
              <VIcon :icon="mdiFilePdfBox"></VIcon>
              <span>PDF</span>
            </VBtn>
            <VBtn :loading="isLoadingSvg" @click="makeSvgZip()">
              <VIcon :icon="mdiFileImage"></VIcon>
              <span>SVG</span>
            </VBtn>
            <VBtn :loading="isLoadingPng" @click="makePngZip()">
              <VIcon :icon="mdiFileImage"></VIcon>
              <span>PNG</span>
            </VBtn>
          </VCardActions>
        </VCard>
      </VCol>
    </VRow>

    <VRow v-if="songReferencesWithSelectedVersifications.length">
      <VCol>
        <VCard>
          <VCardTitle>Preview slides</VCardTitle>
          <VCardText>
            <SlidePreview
              class="mb-4"
              v-for="(slide, index) in slides"
              :key="index"
              :header-left="slide.headerLeft"
              :header-right="slide.headerRight"
              :lyrics="slide.lyrics"
            ></SlidePreview>
          </VCardText>
        </VCard>
      </VCol>
    </VRow>
  </VContainer>
</template>

<script setup lang="ts">
import { chunkEqual } from "@/units/arrays";
import { pad, yyyymmdd } from "@/units/format";
import JSZip from "jszip";
import { svgAsPngUri } from "save-svg-as-png";
import { uriToBlob } from "@/units/misc";
import SlidePreview from "@/components/SlidePreview.vue";
import { mdiFileImage, mdiFilePdfBox } from "@mdi/js";
import { computed, ref, watch } from "vue";
import { useHomeStore } from "@/stores/home";
import saveAs from "file-saver";
import { parseLiturgy } from "@/units/parse";

import {
  getAllVersifications,
  getStanzas,
  getStanzaTitle,
  getVersification,
} from "@/db/util";
import LyricsPreview from "@/components/LyricsPreview.vue";
import { renderSongReference, renderVersification } from "@/units/render";
import type { SongReference } from "@/types";
import type { Versification } from "@/db/types";
import { isDefined } from "@/units/typescriptHelpers";
import pdfMake from "pdfmake/build/pdfmake";
import type { PageOrientation } from "pdfmake/interfaces";
import {
  regularBase64,
  italicBase64,
} from "@/assets/fonts/Cardo/CardoFilesToBase64"

// We have to add the font as a base64 to the vfs to use other custom fonts
pdfMake.vfs = {
  "Cardo-Regular.ttf": regularBase64,
  "Cardo-Italic.ttf": italicBase64,
};

// Define the font in the fonts colletion to be able to use it
pdfMake.fonts = {
  Cardo: {
    normal: "Cardo-Regular.ttf",
    italics: "Cardo-Italic.ttf",
  },
};

const store = useHomeStore();

const songReferencesWithAvailableVersifications = computed<
  { songReference: SongReference; versifications: Versification[] }[]
>(() =>
  parseLiturgy(store.liturgyInput)
    .map((songReference) => ({
      songReference,
      versifications: getAllVersifications(
        songReference.book,
        songReference.number,
        songReference.stanzas,
      ),
    }))
    .filter((r) => r.versifications.length),
);

const songReferencesWithSelectedVersifications = computed<
  Required<SongReference>[]
>(() =>
  songReferencesWithAvailableVersifications.value
    .map(
      (
        { songReference, versifications },
        index,
      ): Required<SongReference> | undefined => {
        const versificationId =
          store.versificationIds[index] ?? versifications[0]?.id;

        if (versificationId === undefined) {
          return undefined;
        }

        return {
          book: songReference.book,
          number: songReference.number,
          stanzas: songReference.stanzas,
          versificationId: versificationId,
        };
      },
    )
    .filter(isDefined),
);

const slides = computed(() =>
  songReferencesWithSelectedVersifications.value
    .map((ref) =>
      getStanzas(ref.book, ref.number, ref.stanzas, ref.versificationId),
    )
    .flat()
    .map((stanza) =>
      chunkEqual(
        // Split on newlines and forward slash.
        stanza.lyrics.split(/[\n/]/g),
        store.maxLinesPerSlide,
      ).map((chunk, chunkIndex, chunks) => {
        const stanzaTitle = getStanzaTitle(
          stanza.song_book,
          stanza.song_number,
          stanza.stanza_number,
          stanza.versification_id,
        );
        const headerLeft =
          chunks.length > 1
            ? `${stanzaTitle} (${chunkIndex + 1} / ${chunks.length})`
            : stanzaTitle;
        const versification = getVersification(stanza.versification_id);
        return {
          headerLeft,
          headerRight: versification
            ? renderVersification(versification.year, versification.author)
            : "",
          lyrics: chunk,
        };
      }),
    )
    .flat(),
);

watch(
  songReferencesWithAvailableVersifications,
  (newValue) => {
    // Fill in default versifications where none is chosen yet.
    store.versificationIds = newValue.map(
      ({ songReference, versifications }, index) => {
        return (
          songReference.versificationId ??
          store.versificationIds[index] ??
          versifications[0]?.id
        );
      },
    );
  },
  {
    immediate: true,
  },
);

function getSvgs(): SVGElement[] {
  return [...document.querySelectorAll(".slide svg.slide__svg")].map(
    function (el) {
      if (!(el instanceof SVGElement)) {
        throw new Error("Expected SVG element.");
      }
      return el;
    },
  );
}

function setVersificationId(
  songIndex: number,
  versificationId: number | null,
): void {
  if (versificationId === null) {
    return;
  }

  const versificationIds = [...store.versificationIds];
  versificationIds[songIndex] = versificationId;
  store.versificationIds = versificationIds;
}

const width = 1600;
const height = 900;

const isLoadingPdf = ref(false);

function makePdf() {
  isLoadingPdf.value = true;
  const svgs = getSvgs();

  const content = svgs.map((svg) => ({
    svg: svg.outerHTML,
    width,
    height,
  }));

  const docDefinition = {
    compress: false,
    pageOrientation: "landscape" as PageOrientation,
    pageSize: {
      width,
      height,
    },
    pageMargins: 0,
    defaultStyle: {
      font: "Cardo",
    },
    content,
  };

  pdfMake
    .createPdf(docDefinition)
    .download(
      `${yyyymmdd(new Date())}_slides.pdf`,
      () => (isLoadingPdf.value = false),
    );
}

const isLoadingSvg = ref(false);

function makeSvgZip() {
  isLoadingSvg.value = true;
  const zip = new JSZip();
  const svgs = getSvgs().map((svg) => svg.outerHTML);
  const padLength = svgs.length.toString().length;
  svgs.forEach((svgSource, index) => {
    zip.file(`${pad(index, padLength)}.svg`, svgSource);
  });
  zip.generateAsync({ type: "blob" }).then((content) => {
    saveAs(content, `${yyyymmdd(new Date())}_slides.zip`);
  });
  isLoadingSvg.value = false;
}

const isLoadingPng = ref(false);

async function makePngZip() {
  isLoadingPng.value = true;
  const pngs = await Promise.all(
    getSvgs().map((svg) =>
      svgAsPngUri(svg).then((uri: string) => uriToBlob(uri)),
    ),
  );
  const zip = new JSZip();
  const padLength = pngs.length.toString().length;
  pngs.forEach((pngData, index) => {
    zip.file(`${pad(index, padLength)}.png`, pngData);
  });
  saveAs(
    await zip.generateAsync({ type: "blob" }),
    `${yyyymmdd(new Date())}_slides.zip`,
  );
  isLoadingPng.value = false;
}
</script>
