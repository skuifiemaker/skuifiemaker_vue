export interface SongReference {
  book: string;
  number: number;
  stanzas: number[];
  versificationId?: number;
}
