/**
 * Pad an integer with zeros, to the given string length.
 *
 * @param x
 *   The integer to pad.
 * @param len
 *   The length of the resulting string.
 *
 * @returns
 *   The padded string representation of x.
 */
export function pad(x: number, len: number): string {
  return ("0".repeat(len) + x).slice(-len);
}

/**
 * Format a date as "yyyymmdd".
 */
export function yyyymmdd(d: Date): string {
  const yyyy = pad(d.getFullYear(), 4);
  const mm = pad(d.getMonth() + 1, 2);
  const dd = pad(d.getDate(), 2);
  return `${yyyy}${mm}${dd}`;
}
