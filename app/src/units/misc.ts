/**
 * Convert a data URI to a blob object.
 *
 * Copied from `saveSvgAsPng.js`, since we can't import it.
 */
export function uriToBlob(uri: string): Blob {
  const byteString = window.atob(uri.split(",")[1]);
  const mimeString = uri.split(",")[0].split(":")[1].split(";")[0];
  const buffer = new ArrayBuffer(byteString.length);
  const intArray = new Uint8Array(buffer);
  for (let i = 0; i < byteString.length; i++) {
    intArray[i] = byteString.charCodeAt(i);
  }
  return new Blob([buffer], { type: mimeString });
}
