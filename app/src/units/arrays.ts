/**
 * Generate an array of integers for range [start;stop).
 *
 * From https://stackoverflow.com/a/44957114/836995
 */
export function range(start: number, stop: number, step: number = 1): number[] {
  return Array(Math.ceil((stop - start) / step))
    .fill(start)
    .map((x, y) => x + y * step);
}

/**
 * Break an array into chunks of specified length. The last chunk will be shorter.
 */
export function chunk<T>(array: T[], chunkLength: number): T[][] {
  return Array(Math.ceil(array.length / chunkLength))
    .fill(null)
    .map((_, index) => index * chunkLength)
    .map((begin) => array.slice(begin, begin + chunkLength));
}

/**
 * Break an array into chunks of similar length, given a maximum chunk size.
 */
export function chunkEqual<T>(array: T[], maxChunkLength: number): T[][] {
  return chunk(array, array.length / Math.ceil(array.length / maxChunkLength));
}
