import { parseLiturgy } from "@/units/parse";
import fs from "fs";
import { describe, test, expect } from "vitest";

describe("parseLiturgy", () => {
  const inputDirectory = `${__dirname}/exampleLiturgies/input`;
  const expectedDirectory = `${__dirname}/exampleLiturgies/expected`;
  fs.readdirSync(inputDirectory).forEach((fileName) => {
    test(`Real world example: ${fileName}`, () => {
      const parsed = parseLiturgy(
        fs.readFileSync(`${inputDirectory}/${fileName}`, "utf8"),
      );
      const expected = JSON.parse(
        fs.readFileSync(
          `${expectedDirectory}/${fileName.replace(/\.txt$/, ".json")}`,
          "utf8",
        ),
      );

      expect(parsed).toMatchObject(expected);
    });
  });
});
