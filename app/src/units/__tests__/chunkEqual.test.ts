import { chunkEqual } from "@/units/arrays";
import { describe, test, expect } from "vitest";

describe("chunkEqual", () => {
  test("It splits an array into equal chunks.", () => {
    expect(chunkEqual([0, 1, 2, 3, 4, 5, 6, 7], 6)).toEqual([
      [0, 1, 2, 3],
      [4, 5, 6, 7],
    ]);
  });
});
