import { parseStanzas } from "../parse";
import { describe, test, expect } from "vitest";

describe("parseStanzas", () => {
  test("Single stanza", () => {
    expect(parseStanzas("1:2")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2],
    });
  });

  test("Comma-separated stanzas", () => {
    expect(parseStanzas("1:2,3")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2, 3],
    });
    expect(parseStanzas("1:2,5")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2, 5],
    });
    expect(parseStanzas("1:2,5,6")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2, 5, 6],
    });
  });

  test("Plus, &, 'en' and 'and' all mean the same thing as a comma:", () => {
    expect(parseStanzas("95 : 1 + 2")).toEqual({
      songNumber: 95,
      stanzaNumbers: [1, 2],
    });
    expect(parseStanzas("95 : 1 + 2+3")).toEqual({
      songNumber: 95,
      stanzaNumbers: [1, 2, 3],
    });
    expect(parseStanzas("95 : 1, 2 en 3")).toEqual({
      songNumber: 95,
      stanzaNumbers: [1, 2, 3],
    });
    expect(parseStanzas("95 : 1, 2 and 3")).toEqual({
      songNumber: 95,
      stanzaNumbers: [1, 2, 3],
    });
    expect(parseStanzas("95 : 1, 2 & 3")).toEqual({
      songNumber: 95,
      stanzaNumbers: [1, 2, 3],
    });
  });

  test("Stanza ranges. Treat hyphens and dashes of various lengths as the same thing.", () => {
    expect(parseStanzas("1:2-3")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2, 3],
    });

    // hyphen
    expect(parseStanzas("1:2-5")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2, 3, 4, 5],
    });

    // en dash
    expect(parseStanzas("1:2–5")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2, 3, 4, 5],
    });

    // em dash
    expect(parseStanzas("1:2—5")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2, 3, 4, 5],
    });
  });

  test("Ignore white-space", () => {
    expect(parseStanzas("\t1 :2-  5    ")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2, 3, 4, 5],
    });
  });

  test("Support repetition of verses", () => {
    expect(parseStanzas("150:1,1")).toEqual({
      songNumber: 150,
      stanzaNumbers: [1, 1],
    });
  });

  test("Support mixture of ranges and individually listed verses", () => {
    expect(parseStanzas("1:2-4,6-7")).toEqual({
      songNumber: 1,
      stanzaNumbers: [2, 3, 4, 6, 7],
    });

    expect(parseStanzas("1:6-7,4,3")).toEqual({
      songNumber: 1,
      stanzaNumbers: [6, 7, 4, 3],
    });

    expect(parseStanzas("1:6-7, 4, 3")).toEqual({
      songNumber: 1,
      stanzaNumbers: [6, 7, 4, 3],
    });
  });

  test("Use empty array to indicate that no stanzas were mentioned explicitly.", () => {
    // This usually means we sing all stanzas.
    expect(parseStanzas("1")).toEqual({
      songNumber: 1,
      stanzaNumbers: [],
    });
  });

  test("Return null when the input is something irrelevant", () => {
    expect(parseStanzas("Genesis 5")).toBeNull();
    expect(parseStanzas("asdf")).toBeNull();
  });
});
