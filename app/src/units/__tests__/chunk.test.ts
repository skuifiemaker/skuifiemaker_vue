import { chunk } from "@/units/arrays";
import { describe, test, expect } from "vitest";

describe("chunk", () => {
  test("It splits an array into chunks.", () => {
    expect(chunk([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3)).toEqual([
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [9, 10],
    ]);
  });
});
