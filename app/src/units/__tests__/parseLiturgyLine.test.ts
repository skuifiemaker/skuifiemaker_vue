import { describe, test, expect } from "vitest";
import { parseLiturgyLine } from "../../units/parse";

describe("Parse user input to create SongReference", () => {
  test("Total rubbish", () => {
    expect(parseLiturgyLine("Asdf MOO")).toBeUndefined();
  });

  test("No space between book and number", () => {
    expect(parseLiturgyLine("Lied512")).toEqual({
      book: "lied",
      number: 512,
      stanzas: [],
    });
  });

  test("Multiple spaces between book and number", () => {
    expect(parseLiturgyLine("PS  1:9")).toEqual({
      book: "ps",
      number: 1,
      stanzas: [9],
    });
  });

  test("No spaces between book and number. Spaces between verse numbers.", () => {
    expect(parseLiturgyLine("Skrifberyming17:2, 3")).toEqual({
      book: "sb",
      number: 17,
      stanzas: [2, 3],
    });
  });

  test("Book, but no song number or verses", () => {
    expect(parseLiturgyLine("Gesange")).toBeUndefined();
  });

  test("Short name, no spaces.", () => {
    expect(parseLiturgyLine("sb50")).toEqual({
      book: "sb",
      number: 50,
      stanzas: [],
    });
  });

  test("Plus between verses", () => {
    expect(parseLiturgyLine("Psalm 5:7+9")).toEqual({
      book: "ps",
      number: 5,
      stanzas: [7, 9],
    });
  });

  test("Ampersand between verses", () => {
    expect(parseLiturgyLine("Psalm 5:7&9")).toEqual({
      book: "ps",
      number: 5,
      stanzas: [7, 9],
    });
  });

  test("The word 'and' between verses.", () => {
    expect(parseLiturgyLine("Psalm 5:7 and 9")).toEqual({
      book: "ps",
      number: 5,
      stanzas: [7, 9],
    });
  });

  test("The word 'en' (Afrikaans) between verses.", () => {
    expect(parseLiturgyLine("Psalm 5:7 en 9")).toEqual({
      book: "ps",
      number: 5,
      stanzas: [7, 9],
    });
  });

  test("Geloofsbelydenis.", () => {
    const ref = { book: "geloofsbelydenis", number: 1, stanzas: [1] };
    expect(parseLiturgyLine("Geloofsbelydenis")).toEqual(ref);
    expect(parseLiturgyLine("Apostoliese Geloofsbelydenis (sing)")).toEqual(
      ref,
    );
  });

  test("With an arbitrary preamble before the song reference.", () => {
    expect(parseLiturgyLine("Sing: Ps 19:1,2")).toEqual({
      book: "ps",
      number: 19,
      stanzas: [1, 2],
    });
  });

  test("Some people abbreviate 'Skrifberyming' as 'Skb'.", () => {
    expect(parseLiturgyLine("Sang: Skb 33:1,3")).toEqual({
      book: "sb",
      number: 33,
      stanzas: [1, 3],
    });
  });

  test("Trailing punctuation is ignored.", () => {
    expect(parseLiturgyLine("Skrifberyming 33:1,3;")).toEqual({
      book: "sb",
      number: 33,
      stanzas: [1, 3],
    });
  });
});
