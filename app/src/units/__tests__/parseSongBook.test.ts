import { parseSongBook } from "../parse";
import { describe, test, expect } from "vitest";

describe("parseSongBook", () => {
  test("Book and number.", () => {
    expect(parseSongBook("Lied 512")).toEqual({
      songBook: "lied",
      stanzas: "512",
    });
  });

  test("Book, number and stanza. No spaces.", () => {
    expect(parseSongBook("PS1:9")).toEqual({
      songBook: "ps",
      stanzas: "1:9",
    });
  });

  test("Book, number and stanzas. Extra spaces.", () => {
    expect(parseSongBook("Skrifberyming  17:2, 3")).toEqual({
      songBook: "sb",
      stanzas: "17:2, 3",
    });
  });

  test("Book, but no number or stanzas.", () => {
    expect(parseSongBook("Gesange")).toEqual({
      songBook: "ges",
      stanzas: "",
    });
  });

  test("Number and stanzas. No book.", () => {
    expect(parseSongBook("5:6-7")).toBeNull();
  });

  test("Short book name and number. No spaces.", () => {
    expect(parseSongBook("sb50")).toEqual({
      songBook: "sb",
      stanzas: "50",
    });
  });

  test("Geloofsbelydenis.", () => {
    expect(parseSongBook("Geloofsbelydenis")).toEqual({
      songBook: "geloofsbelydenis",
      stanzas: "",
    });

    expect(parseSongBook("Apostoliese Geloofsbelydenis (sing)")).toEqual({
      songBook: "geloofsbelydenis",
      stanzas: "(sing)",
    });
  });

  test("With an arbitrary preamble before the song reference.", () => {
    expect(parseSongBook("Sing: Ps 19:1,2")).toEqual({
      songBook: "ps",
      stanzas: "19:1,2",
    });

    expect(parseSongBook("Lofpsalm: Ps 19:1,2")).toEqual({
      songBook: "ps",
      stanzas: "19:1,2",
    });
  });

  test("Some people abbreviate 'Skrifberyming' as 'Skb'.", () => {
    expect(parseSongBook("Sang: Skb 33:1,3")).toEqual({
      songBook: "sb",
      stanzas: "33:1,3",
    });
  });

  test("Trailing punctuation does not break the function.", () => {
    expect(parseSongBook("Skrifberyming 33:1,3;")).toEqual({
      songBook: "sb",
      stanzas: "33:1,3;",
    });
  });
});
