export default {
  // Separates lists of stanzas.
  splitStanzaList: /,|\+|en|and|&/,

  // Separates the "from" and "to" parts of stanza ranges.
  splitStanzaRange: /-|–|—|to|tot/,

  // Separates the song references from the numbers and stanzas.
  songReferences: [
    {
      re: /.*?(?<!\w)(apostoliese)?\s*geloof(sb(elydenis)?)?\s*(.*)/i,
      book: "geloofsbelydenis",
    },
    {
      re: /.*?(?<!\w)ps(alm(s)?)?\s*(.*)/i,
      book: "ps",
    },
    {
      re: /.*?(?<!\w)((sk?b)|(skrifb(er(ym(ing(s)?)?)?)?))\s*(.*)/i,
      book: "sb",
    },
    {
      re: /.*?(?<!\w)ges(ang(e)?)?\s*(.*)/i,
      book: "ges",
    },
    {
      re: /.*?(?<!\w)l(ied(ere|boek)?)?\s*(.*)/i,
      book: "lied",
    },
  ],
};
