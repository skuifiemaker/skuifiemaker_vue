import regexes from "@/units/regexes";
import { range } from "@/units/arrays";
import { isDefined } from "@/units/typescriptHelpers";
import type { SongReference } from "@/types";

/**
 * Parse the user-provided liturgy.
 *
 * @param inputLiturgy
 *
 * @return
 *   An array of parsed song references. Unknown references are omitted.
 */
export function parseLiturgy(inputLiturgy: string): SongReference[] {
  return inputLiturgy
    .split("\n")
    .filter((line) => line.trim().length)
    .map(parseLiturgyLine)
    .filter(isDefined);
}

/**
 * Create a song reference object by parsing a line from user-provided liturgy.
 */
export function parseLiturgyLine(
  inputLine: string,
): Pick<SongReference, "book" | "number" | "stanzas"> | undefined {
  const parsedBook = parseSongBook(inputLine) || {
    songBook: null,
    stanzas: "",
  };

  // Special handling for "apostoliese geloofsbelydenis".
  if (parsedBook.songBook === "geloofsbelydenis") {
    return {
      book: "geloofsbelydenis",
      number: 1,
      stanzas: [1],
    };
  }

  const parsedStanzas = parseStanzas(parsedBook.stanzas) || {
    songNumber: null,
    stanzaNumbers: [],
  };

  if (parsedBook.songBook === null || parsedStanzas.songNumber === null) {
    return undefined;
  }
  return {
    book: parsedBook.songBook,
    number: parsedStanzas.songNumber,
    stanzas: parsedStanzas.stanzaNumbers,
  };
}

/**
 * Split line into the songbook name and the string providing the song number and stanzas.
 */
export function parseSongBook(
  inputLine: string,
): { songBook: string; stanzas: string } | null {
  for (const { re, book } of regexes.songReferences) {
    const matches = re.exec(inputLine);
    if (!matches) {
      continue;
    }

    return {
      // Stanzas are in the last group
      stanzas: matches[matches.length - 1],
      songBook: book,
    };
  }

  return null;
}

/**
 * Parse a string to find song number and stanza numbers.
 *
 * This is different from parsing chapters and verses of bible books, for two reasons:
 * - We don't have to support crossing chapter boundaries, like 1:2-3:4
 * - We have to support singing verses in arbitrary order, like 2:3,2,6-7
 *
 * @param stanzas
 *   The human-readable song reference.
 */
export function parseStanzas(
  stanzas: string,
): { songNumber: number; stanzaNumbers: number[] } | null {
  const [songNumberString, theRest] = stanzas.split(":", 2);
  const songNumber = parseInt(songNumberString);
  if (!songNumber) {
    return null;
  }

  if (!theRest) {
    return {
      songNumber: songNumber,
      stanzaNumbers: [],
    };
  }

  const rangeStrings = theRest.split(regexes.splitStanzaList);
  const stanzaNumbers = rangeStrings.reduce(
    (acc: number[], rangeString: string) => {
      const [fromStanza, toStanza] = rangeString
        .split(regexes.splitStanzaRange, 2)
        .map((i) => parseInt(i));
      return [
        ...acc,
        ...range(
          fromStanza,
          (Number.isInteger(toStanza) ? toStanza : fromStanza) + 1,
        ),
      ];
    },
    [],
  );

  return {
    songNumber: songNumber,
    stanzaNumbers,
  };
}
