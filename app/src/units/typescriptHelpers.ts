/**
 * Check if the given value is defined and not `null`.
 *
 * Useful as a callback function like `arr.filter(isDefined)` to narrow `T | undefined | null` to `T`.
 *
 * See https://stackoverflow.com/a/62753258/836995
 */
export function isDefined<T>(val: T | undefined | null): val is T {
  return val !== undefined && val !== null;
}
