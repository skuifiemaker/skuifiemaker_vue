import { getSongTitle } from "@/db/util";

/**
 * Render a human-readable song reference.
 */
export function renderSongReference(
  book: string,
  number: number,
  stanzaNumbers: number[],
): string {
  const title = getSongTitle(book, number)?.title || "Unknown title";
  const stanzas = stanzaNumbers.join(",");
  return stanzas ? `${title}: ${stanzas}` : title;
}

/**
 * Render a human-readable versification.
 */
export function renderVersification(year: number, author: string): string {
  // Try not to make the slide ugly with things like "?" and "N/A".
  const hide_values = ["?", "N/A"];
  if (hide_values.includes(author)) {
    author = "";
  }

  return year ? `${author}, ${year}` : author;
}
