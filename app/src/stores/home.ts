import { ref } from "vue";
import { defineStore } from "pinia";

export const useHomeStore = defineStore(
  "skuifiemaker",
  () => {
    const liturgyInput = ref<string>("");
    const versificationIds = ref<(number | undefined)[]>([]);
    const maxLinesPerSlide = ref<number>(6);

    return {
      liturgyInput,
      versificationIds,
      maxLinesPerSlide,
    };
  },
  {
    persist: true,
  },
);
