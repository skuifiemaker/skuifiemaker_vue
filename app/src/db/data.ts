import versificationsJson from "./tables/versifications.json";
import stanzasJson from "./tables/stanzas.json";
import titlesJson from "./tables/titles.json";
import type { Stanza, Title, Versification } from "@/db/types";

export const versifications: Versification[] = versificationsJson;
export const stanzas: Stanza[] = stanzasJson;
export const titles: Title[] = titlesJson;
