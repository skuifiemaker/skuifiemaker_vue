import type { Stanza, Title, Versification } from "@/db/types";
import * as db from "@/db/data";

export function getStanzaTitle(
  song_book: string,
  song_number: number,
  stanza_number: number,
  versificationId: number,
): string {
  const songTitle = getSongTitle(song_book, song_number)?.title ?? "";

  if (getStanzaSiblings(song_book, song_number, versificationId).length === 1) {
    // There is only one stanza in this song, so don't show the stanza number.
    return songTitle;
  }

  return `${songTitle}: ${stanza_number}`;
}

function getStanzaSiblings(
  songBook: string,
  songNumber: number,
  versificationId: number,
): Stanza[] {
  return db.stanzas.filter((stanza) => {
    return (
      stanza.song_book === songBook &&
      stanza.song_number === songNumber &&
      stanza.versification_id === versificationId
    );
  });
}

export function getSongTitle(book: string, number: number): Title | undefined {
  return db.titles.find(
    (t) => t.song_book === book && t.song_number === number,
  );
}

export function getVersification(id: number): Versification | undefined {
  return db.versifications.find((v) => v.id === id);
}

/**
 * Get all available versifications for the referenced stanzas, regardless of the referenced versification.
 */
export function getAllVersifications(
  book: string,
  number: number,
  stanzas: number[],
): Versification[] {
  const allStanzas = getAllStanzas(book, number, stanzas);

  return db.versifications.filter((versification) => {
    return !!allStanzas.find(
      (stanza) => stanza.versification_id === versification.id,
    );
  });
}

/**
 * Get referenced stanzas for the selected versification.
 */
export function getStanzas(
  book: string,
  number: number,
  stanzas: number[],
  versificationId: number,
): Stanza[] {
  return getAllStanzas(book, number, stanzas).filter(
    (stanza) => stanza.versification_id === versificationId,
  );
}

/**
 * Get all referenced stanzas for all versifications of the referenced song.
 */
function getAllStanzas(
  book: string,
  number: number,
  stanzas: number[],
): Stanza[] {
  return db.stanzas.filter((stanza) => {
    return (
      stanza.song_book === book &&
      stanza.song_number === number &&
      // If this.stanzas is an empty array, it means ALL stanzas, rather than NO stanzas.
      (!stanzas.length || stanzas.includes(stanza.stanza_number))
    );
  });
}
