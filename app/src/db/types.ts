export interface Versification {
  id: number;
  author: string;
  year: number;
  language: string;
  description: string;
}

export interface Stanza {
  id: number;
  song_book: string;
  song_number: number;
  versification_id: number;
  stanza_number: number;
  lyrics: string;
  heading: string | null;
}

export interface Title {
  id: number;
  song_book: string;
  song_number: number;
  title: string;
}
