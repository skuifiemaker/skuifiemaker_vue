# Vue front end for Dolf's skuifiemaker app

This app generates slides with lyrics for songs, mainly for use in church.

## Project setup

```
yarn install
```

## Serve for development

```
yarn serve
```

## Build for production

```
yarn build
```

When serving the app from a subfolder:

```
PUBLIC_PATH=/asdf/ yarn build
```

## Run unit tests

```
yarn test:unit
```

## Lint and fix files

```
yarn lint
```
